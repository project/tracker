# Activity Tracker

Enables tracking of recent content for users

The Activity Tracker core module displays a site's most recently added
or updated content. The Activity Tracker module also provides user-level
tracking, which allows you to follow the contributions of specific authors.

For a full description of the module visit
[project page](https://www.drupal.org/project/tracker)

To submit bug reports and feature suggestions, or to track changes visit
[issue queue](https://www.drupal.org/project/issues/tracker)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.
When enabled, the module start tracking the activities.


## Maintainers

- Naveen Valecha - [naveenvalecha](https://www.drupal.org/u/naveenvalecha)
- Andrey Postnikov - [andypost](https://www.drupal.org/u/andypost)
- Derek Wright - [dww](https://www.drupal.org/u/dww)
